# Ethical Contributor Licence Agreement

**created by [Pixelcode](https://keyoxide.org/hkp/aeef8cdb5adf2f28016f39e1fbfc237daf98d402) for Free Ethical Software**
<br><i>version 0.1, August 2022</i>

## Acceptance

This CLA is automatically offered to every person and entity subject to its terms and conditions. The Contributor accepts this CLA and agrees to its terms and conditions by contributing to the Licensed Work.

## Permission

The Contributor grants by this CLA to the Licensor permission to incorporte the Contribution into the Licensed Work and to use the Contribution according to the terms of the Licensed Work’s Licence; subject to all of the terms and conditions of this CLA.

## Re-licensing

After incorporating the Contribution into the Licensed Work, the Licensor may relicense the Licensed Work under a licence different from the current Licence, provided that the terms of such new licence are comparable with those of the current Licence and do not grossly contradict the intention underlying the current Licence.

## Legality

The Contributor affirms that contributing his/her Contribution does not constitute an infringement of any third-party copyrights or any other intellectual property rights.

## Definitions

### CLA

“CLA” means “Contributor Licence Agreement”.

### Contributor

The Contributor is any person or entity contributing to the Licensed Work.

### Contributing

Contributing means willingly proposing to the Licensor one’s own contributions to be incorporated into the Licensed Work, in particular, if it is a software work, via the source code repository managed by the Licensor as well as related issue trackers, pull request managers or version control systems.

### Contribution

Contribution means any copyright-protected work of the Contributor which he/she willingly contributes to the Licensed Work.

### Licensor

The Licensor is the party that has applied this CLA to the Licensed Work by virtue of its copyright.

### Licensed Work

The Licensed Work is that work of the Licensor to which he/she has applied this CLA.

### Licence

The Licence is the current licence under which the Licensor has placed the Licensed Work.