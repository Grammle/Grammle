# Grammle – häufige Grammatikfehler

Grammle bietet eine kurze und knackige Übersicht über häufige sprachliche Fehler. Damit soll Grammle es ermöglichen, eigenen sprachlichen Unsicherheiten zu begegnen. Fragt man sich regelmäßig, ob es „aufwändig“ oder „aufwendig“ heißt, könnte man sich einfach die entsprechende Regel als Lesezeichen abspeichern.

Grammle kann auch beim Korrekturlesen fremder Texte hilfreich sein, um Änderungen von Formulierungen kurz und prägnant zu begründen. Statt lang und breit selbst darlegen zu müssen, warum ein bestimmter Ausdruck lieber vermieden werden sollte, kann der Lektor einfach auf die entsprechende Grammle-Regel verweisen.

[![„Wider besseren Wissens ging er nach draußen, wo das Unwetter bereits erbarmungslos tobte.“ – Schön geschrieben. Trotzdem falsch. Darum nächstes Mal bei Grammle nachsehen.](https://codeberg.org/Grammle/Grammle/raw/branch/master/assets/banner.png)](https://grammle.codeberg.page)

## Voraussetzungen zum Bauen

Um Grammle selbst bauen zu können, musst du:

1. Die neueste Hugo-Extended-Version von [GitHub](https://github.com/gohugoio/hugo/releases) als `.deb` herunterladen.
2. Die heruntergeladene `.deb` installieren (dabei den richtigen Dateinamen verwenden!).
3. Git, Node.js und Python 3 installieren.
4. Das Grammle-Repository klonen.
5. Einen Ausgabe-Ordner namens `Grammle-pages` anlegen und dort ein neues Git-Repo erzeugen.

```
$ sudo apt install ./hugo_extended_VERSION_Linux-64bit.deb
$ sudo apt install git nodejs python3
$ git clone https://codeberg.org/Grammle/Grammle.git
$ mkdir Grammle-pages && cd Grammle-pages && git init && cd ../Grammle
```

## Grammle bauen

Die Informationen zu den einzelnen Grammatikregeln sind in der Datei `regeln.yaml` gespeichert. Sie werden zunächst mithilfe von [Asaf Zamirs](https://www.kidsil.net) `hugo.js` aus [Hugo-Data-to-Pages](https://github.com/kidsil/hugo-data-to-pages) in einzelne Markdown-Seiten umgewandelt. Anschließend wird daraus mit dem fantastischen Static-Site-Generator [Hugo](https://gohugo.io) die Website fertiggebaut. Mit dem `serve`-Befehl kann eine Live-Vorschau der Website unter `localhost` gestartet werden. `grammle.sh` baut Grammle automatisch fertig (und gibt dabei alle angegebenen Parameter an Hugo weiter). Lautet der erste Parameter `commit`, wird zusätzlich das Skript `commit.sh` ausgeführt, mit welchem Änderungen am Git-Repository committed und gepusht werden können.

### Automatisch

```
$ bash grammle.sh
$ bash grammle.sh serve
$ bash grammle.sh commit
```

### Manuell

```
$ rm -r content/regeln
$ node hugo.js generate
# Über den Datei-Explorer den Ausgabe-Ordner "Grammle-pages" leeren, aber den versteckten Ordner .git behalten!
$ hugo
$ hugo serve
$ git add .
$ git commit
$ git push origin master
$ cd ../../Grammle-pages
$ git add .
$ git commit
$ git push origin master
```

## Kann ich mitmachen?

Klar! Feedback, Kritik und Ergänzungen sind immer willkommen, und auch Vorschläge für ganz neue Grammle-Einträge kannst du gern machen. Gehe dazu zur [Bearbeitungsseite](https://grammle.codeberg.page/bearbeiten), speichere deine Änderungen in eine Datei und sende diese an den Autor.

Öffne dazu einfach einen neuen [Issue](https://codeberg.org/Grammle/Grammle/issues) bzw. Pull-Request auf Codeberg oder schreibe eine Direktnachricht per [Mail](mailto:pixelcode@dismail.de) oder [Matrix-Messenger](https://matrix.to/#/@pixelcode:tchncs.de).

Durch das Beiträgen zu Grammle stellst du deine Änderungen unter die Repository-Lizenzen (siehe unten), wobei das [Ethical CLA](Ethical-CLA.md) gilt.

Folge Grammle auf <a href="https://bildung.social/@grammle" rel="me">Mastodon</a>!

## Lizenzen

Unter der Voraussetzung, dass

1. der Titel „Grammle“ sowie Grammle-Logos, -Icons, -Banner, -Werbeanzeigen und andere Marketing-Medien etc. entfernt oder zumindest hinreichend abgeändert werden, sodass einer potenziellen Verwechslung zwischen dem Original des Lizenzgebers und der Adaption des Lizenznehmers wirksam vorgebeugt wird,

2. nicht der falsche Eindruck erweckt wird, der Lizenzgeber unterstütze oder bewerbe explizit die Adaption des Lizenznehmers oder den Lizenznehmer selbst, und dass

3. keine der nachstehend genannten Inhalte zum Zwecke der üblen Nachrede, Verleumdung, falschen Verdächtigung, Rufschädigung usw. gegen den Lizenzgeber verwendet werden,

dürfen

* a) sämtliche von Pixelcode erstellten Inhalte im auf Codeberg.org zu findenden [Grammle-Repository](https://codeberg.org/Grammle/Grammle) gemäß der Bestimmungen der [For Good Eyes Only Licence v0.2](https://codeberg.org/Grammle/Grammle/raw/branch/master/For%20Good%20Eyes%20Only%20Licence%20v0.2.pdf) genutzt werden und

* b) die von Pixelcode erstellten Grammle-Inhalte gemäß der Creative-Commons-Lizenz Attribution-ShareAlike 4.0 International ([CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0)) genutzt werden, wobei

als „Grammle-Inhalte“ alle Inhalte der gerenderten [Grammle-Website](https://grammle.de) gelten, die ein potenzieller Nutzer visuell oder akustisch warnehmen kann (Texte, Bilder, Videos, Audio etc.). Dazu zählen insbesondere die Inhalte der [Grammle-Regeln](https://grammle.de/regeln) (sowohl einzeln als auch als Sammlung) und Meta-Texte wie z.B. „[Über Grammle](https://grammle.de/über)“. Nicht dazu zählt hingegen maschinenlesbarer, ausführbarer Programmcode (z.B. JavaScript).

Es ist deutsches Recht anzuwenden.

[![For Good Eyes Only Flag](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/commit/d334335d5b422417175d8362ad92ad68de663295/assets/banner/Banner%20250.png)](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only)

[![summary](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only/raw/branch/master/assets/summary/Summary%20v0.2%20750px.png)](https://codeberg.org/ForGoodEyesOnly/for-good-eyes-only)

## Inhalte von Dritten

Grammle verwendet folgende Inhalte, die (größtenteils) von Dritten erstellt worden sind und/oder unter einer anderen Lizenz stehen:

* `hugo.js` von Asaf Zamir ([MIT-Lizenz](foreign_licences/Hugo-Data-to-Pages.md))
* Bootstrap-Icons von The Bootstrap Authors ([MIT-Lizenz](foreign_licences/Bootstrap.md))
* Font-Awesome-Icons von Fonticons Inc. ([Font-Awesome-Free-Lizenz](foreign_licences/Font-Awesome.md))
* Inter von Rasmus Andersson [Open-Font-Lizenz](foreign_licences/OFL.txt)
* Merriweather von Sorkin Type [Open-Font-Lizenz](foreign_licences/OFL.txt)
* Patua One von LatinoType [Open-Font-Lizenz](foreign_licences/OFL.txt)
* Ordner `node_modules` (Lizenzhinweise im jeweiligen Unterordner)
* Ordner `img` (teilweise von Dritten)
* `grammle.sh` und `commit.sh` von Pixelcode ([MIT-Lizenz](https://codeberg.org/pixelcode/HugoCommit.sh/src/branch/master/LICENCE.md), leicht abgewandelt)