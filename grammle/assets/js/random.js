$('.random-rule-button').addClass('random-rule-button-visible');
$('.card-random-container').addClass('card-random-container-visible');

var ruleURLs = [ {{ range (where site.RegularPages "Section" "regeln") }}"{{ .Permalink }}",{{ end }} ""];

ruleURLs.pop(); // remove empty URL at end of array
console.log(ruleURLs);

var currentURL = window.location.href;
var randomURL = getRandomURL();

while (randomURL == currentURL) {
  randomURL = getRandomURL();
}

function getRandomURL() {
  return ruleURLs[ Math.floor(Math.random() * ruleURLs.length) ]; // https://stackoverflow.com/a/4550514
}

$(".random-rule-button").click(function() {
  window.location.href = randomURL;
});