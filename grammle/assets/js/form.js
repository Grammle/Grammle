$('.form-section').addClass('form-section-visible');
$('.form-section-js-error').removeClass('form-section-visible');

var mistakes = new Object();
var catMistakes = new Object();
var mistakeCategoriesTemplate = $('#mistake-category-template').html();
var mistakeItemsTemplate = $('#mistake-item-template').html();

// If user leaves edit form, ask for confirmation
$(window).bind('beforeunload', function(){
  if (mistakes.articles.length > 0) {
    return 'Bist du sicher, dass du Grammle nicht weiter bearbeiten möchtest? Deine Änderungen könnten verloren gehen!';
  }
});


// -------------------
// SORT MISTAKES BY ID
// -------------------
function sortMistakes() {
  console.log('attempting to sort the  mistakes');

  // alphabetically sort mistakes by ID
  mistakes.articles.sort((a, b) => {
    var aCat = a.fields.id[0];
    var bCat = b.fields.id[0];
    var aID = parseInt(a.fields.id.slice(1));
    var bID = parseInt(b.fields.id.slice(1));

    if (aCat < bCat) {
      return -1;

    } else if (aCat > bCat) {
      return 1;

    } else if (aCat == bCat) {
      if (aID < bID) return -1;
      if (aID > bID) return 1;
    }

    return 0;
  });

  catMistakes = {};

  // for all mistakes
  for (i=0; i < mistakes.articles.length; i++) {
    var currentMistake = mistakes.articles[i];
    var currentCategory = currentMistake.fields.kategorien[0];

    // if current category is not yet included in categories array
    if (!( currentCategory in catMistakes )) {
      var currentCatMistakes = [];
      catMistakes[currentCategory] = currentCatMistakes;
    }

    catMistakes[currentCategory].push(currentMistake);
  }
}


// ------------------------
// DISPLAY LIST OF MISTAKES
// ------------------------
function listIDs() {
  console.log('attempting to list all IDs');
  $('.form-error-mistake-items').addClass('form-error-hidden');
  clearList();

  // for every category
  for (category in catMistakes) {
    var categoryHTML = mistakeCategoriesTemplate; // defined at the top
    categoryHTML = categoryHTML.replaceAll("::CATEGORY::", category);
    $('.mistake-categories').append(categoryHTML);

    // for every mistake in category
    for (i = 0; i < catMistakes[category].length; i++) {
      var mistake = mistakeItemsTemplate;

      mistake = mistake.replaceAll("::ID::", catMistakes[category][i].fields.id);
      mistake = mistake.replaceAll("::SLUG::", catMistakes[category][i].fields.slug);

      $('.mistake-category[item="' + category + '"]').find('.mistake-items').append(mistake);
    }
  }

  toggleOutputButton(true);
}


// ----------------------
// ENABLE GENERATE BUTTON
// ----------------------
function toggleOutputButton(targetStatus) {
  if (targetStatus) {
    $('.file-output-button').removeClass('form-button--disabled');
    $('.file-output-button').addClass('form-button--enabled');
    return;
  }

  $('.file-output-button').addClass('form-button--disabled');
  $('.file-output-button').removeClass('form-button--enabled');
}


// ---------------------------
// CLEAR CURRENT MISTAKES LIST
// ---------------------------
function clearList() {
  $('.mistake-category').remove();
  $('.mistake-categories').text('');
  $('.mistake-categories').html(mistakeCategoriesTemplate);

  toggleOutputButton(false);
}


// ----------------------------------------
// FIND MISTAKE IN MISTAKES OBJECT BY PARAM
// ----------------------------------------
function findMistakeInCategoryByParam(category, match, param) {
  console.log('attempting to find mistake with ' + param + ' == ' + match + ' in ' + category);
  var mistakeIndex = -1;

  // for every mistake in category
  for (i = 0; i < catMistakes[category].length; i++) {

    // if current ID matches
    if (catMistakes[category][i].fields[param] == match) {
      mistakeIndex = i;
      break;
    }
  }

  return mistakeIndex;
}


// find mistake in mistakes object by id
function findMistakeInCategoryByID(category, mistakeID) {
  return findMistakeInCategoryByParam(category, mistakeID, "id");
}

// find mistake in mistakes object by UID
function findMistakeInCategoryByUID(category, mistakeUID) {
  return findMistakeInCategoryByParam(category, mistakeUID, "uid");
}

// We're given a value "valueGiven" of a parameter "paramGiven".
// Our task is to find the first match in the mistakes object
// and return the value of the matching element's parameter "paramLookedFor".
function getParamByParam(valueGiven, paramGiven, paramLookedFor) {
  console.log('attempting to get param ' + paramLookedFor + ' where ' + paramGiven + ' == ' + valueGiven);
  var i = 0;

  for (i = 0; i < mistakes.articles.length; i++) {
    var currentValueOfParamGiven = mistakes.articles[i].fields[paramGiven];
    var valueOfParamLookedFor    = mistakes.articles[i].fields[paramLookedFor];
    if (currentValueOfParamGiven == valueGiven) return valueOfParamLookedFor;
  }

  return -1;
}

// get the UID of a mistake known by its ID
function getUIDbyID(id) {
  return getParamByParam(id, "id", "uid");
}

// get the ID of a mistake known by its UID
function getIDbyUID(uid) {
  return getParamByParam(uid, "uid", "id");
}

// return the highest ID present in a given category
function getHighestIDinCategory(category) {
  console.log('attempting to get the highest ID in ' + category);
  var id = 0;
  var i = 0;

  // simply iterate until the latest element…
  for (i = 0; i < category.length; i++) {
    id = category[i].fields.id;
  }

  return id; // …and return its ID
}

// find a mistake in a category by its ID
function getIndexOfMistakeInCategory(category, id) {
  console.log('attempting to get index of ' + id + ' in ' + category);

  for (var i = 0; i < category.length; i++) {
    var currentID = category[i].fields.id;
    if (currentID == id) return i; // we found the ID, so we return the index
  }

  return -1; // we didn't find the ID, so we return -1
}


// ------------------------------------
// MERGE ALL MISTAKES OF ALL CATEGORIES
// ------------------------------------
function mergeCategories() {
  console.log('attempting to merge all categories');
  mistakes = { articles: [] };

  // for all categories
  for (category in catMistakes) {
    if (catMistakes.hasOwnProperty(category)) {

      // for all mistakes in category
      for (i = 0; i < catMistakes[category].length; i++) { // no idea why it must be length here instead of length-1
        var currentMistake = catMistakes[category][i];
        mistakes["articles"].push(currentMistake);
      }

    }
  }
}