function newMistake(category) {
  console.log('attempting to create new mistake in ' + category);
  var highestIDinCat = 0;

  // for each mistake category
  for (i = 0; i < catMistakes[category].length - 1; i++) {
    var currentIDstring = catMistakes[category][i].fields.id; // extract the highest ID present in the category
    highestIDinCat = parseInt(currentIDstring.slice(1)) + 1;  // human counting from 1
  }

  var newHighestIDinCat = highestIDinCat + 1; // new highest ID in category = currently highest ID + 1
  var id = category[0] + newHighestIDinCat;   // id := category letter + new highest ID in category
  var orderID = category[0] + newHighestIDinCat.toString().padStart(4, '0');

  var pseudoMistake = {
    path: id,
    fields: {
      id: id,
      uid: 'UID unspecified',
      orderID: orderID,
      name: 'Grammle-' + id.toUpperCase(),
      aliases: [id],
      arten: 'fehler',
      kategorien: [category],
      source: {}
    }
  }

  addMistake(category, pseudoMistake);
  singleEditMistake(category, pseudoMistake.fields.id);
  deleteMistake(category, pseudoMistake.fields.id);
}

// -------------------
// EDIT SINGLE MISTAKE
// -------------------
function singleEditMistake(category, id) {
  console.log('attempting to open edit popup');
  $('.mistake-single-edit').removeClass('mistake-single-edit--hidden');
  $('body').css({ "overflow-y": "hidden" });

  $('.mistake-edit-input-radio').prop('checked', false);
  $('.mistake-edit-input-checkbox').prop('checked', false);

  var mistakeIndex = findMistakeInCategoryByID(category, id);
  var mistake = catMistakes[category][mistakeIndex];

  var name        = mistake.fields.name;
  var mistakeID   = mistake.fields.id;
  var mistakeUID  = mistake.fields.uid;
  var slug        = mistake.fields.slug;
  var aliases     = mistake.fields.aliases;
  var arten       = mistake.fields.arten;
  var kategorie1  = mistake.fields.kategorien[0];
  var wrong       = mistake.fields.wrong;
  var wrongex     = mistake.fields.wrongex;
  var correct     = mistake.fields.correct;
  var correctex   = mistake.fields.correctex;
  var wrongs      = mistake.fields.wrongs;
  var explanation = mistake.fields.explanation;
  var note        = mistake.fields.note;

  $('.mistake-single-edit-form').attr('grammle-id', mistakeID);
  $('.mistake-single-edit-form').attr('grammle-uid', mistakeUID);
  $('.mistake-single-edit-form').attr('grammle-category', kategorie1);
  $('.mistake-single-edit-form').attr('grammle-nr', mistakeID.slice(1));
  $('.mistake-single-edit-headline').text(name);
  $('#mistake-edit-slug').val(slug);
  $('#mistake-edit-kind-' + arten).prop("checked", true);
  $('#mistake-edit-maincat').val(kategorie1);
  $('#mistake-edit-wrong').val(wrong);
  $('#mistake-edit-wrongex').val(wrongex);
  $('#mistake-edit-correct').val(correct);
  $('#mistake-edit-correctex').val(correctex);
  $('#mistake-edit-explanation').val(explanation);
  $('#mistake-edit-note').val(note);

  // fill in UID if provided, else don't show it
  if (mistakeUID.length == 32) {
    $('.mistake-single-edit-headline-uid').removeClass('uid-hidden');
    $('.mistake-single-edit-headline-uid').text(mistakeUID);
    $('.mistake-single-edit-headline-uid').attr('title', 'UID: ' + mistakeUID);
  } else {
    $('.mistake-single-edit-headline-uid').addClass('uid-hidden');
  }

  for (i = 0; i < mistake.fields.kategorien.length; i++) {
    $('#mistake-edit-cat-' + mistake.fields.kategorien[i]).prop("checked", true);
  }

  enterAliases(mistake);
  enterAlt(mistake);
  enterConfusion(mistake);
  enterSource(mistake);
  enterRelatedRules(mistake);
  enterWrongs(mistake);
}


// ----------------------------------------------
// ENTER ITEMS OF SPECIFIED ARRAY INTO EDIT POPUP
// ----------------------------------------------
function enterItemsOfArray(mistake, arrayName) {
  console.log('attempting to enter the items of ' + arrayName + 'into the mistake');
  $('#mistake-edit-' + arrayName).val('');
  if (mistake.fields[arrayName] == undefined) return;

  // for every alternative in array
  for (i = 0; i < mistake.fields[arrayName].length; i++) {

    if (arrayName == "source") {
      var sourceName = Object.keys(mistake.fields.source[i])[0];
      var value = $('#mistake-edit-source').val() + sourceName + ': ' + mistake.fields.source[i][sourceName];

    } else if (arrayName == "related") {
      var uid   = mistake.fields[arrayName][i];
      var id    = getIDbyUID(uid);
      var value = $('#mistake-edit-' + arrayName).val() + id;

    } else {
      var value = $('#mistake-edit-' + arrayName).val() + mistake.fields[arrayName][i];
    }

    // only add 2 line breaks if it's not the last element
    if (i < mistake.fields[arrayName].length - 1) {
      $('#mistake-edit-' + arrayName).val(value + '\n\n');
    } else {
      $('#mistake-edit-' + arrayName).val(value);
    }
  }
}


// enter aliases into edit popup
function enterAliases(mistake) {
  enterItemsOfArray(mistake, "aliases");
}

// enter alt into edit popup
function enterAlt(mistake) {
  enterItemsOfArray(mistake, "alt");
}

// enter confusion value into edit popup
function enterConfusion(mistake) {
  enterItemsOfArray(mistake, "confusion");
}

// enter related rules into edit popup
function enterRelatedRules(mistake) {
  enterItemsOfArray(mistake, "related");
}

// enter regular expressions for wrongs into edit popup
function enterWrongs(mistake) {
  enterItemsOfArray(mistake, "wrongs");
}

// enter sources into edit popup
function enterSource(mistake) {
  enterItemsOfArray(mistake, "source");
}


// -------------------------
// CREATE TOAST NOTIFICATION
// -------------------------
function toast(status, text) {
  $('.toast').addClass('toast--' + status).addClass('toast--show');
  $('.toast-text').text(text);

  setTimeout(function() {
    $('.toast').removeClass('toast--show');
  }, 5000);
}


// -----------------------------
// CHECK WHETHER EDITS ARE VALID
// -----------------------------
function validateEdits() {
  console.log('attempting to validate edits');

  if ($('#mistake-edit-source').val()    == '' ||
      $('#mistake-edit-wrong').val()     == '' ||
      $('#mistake-edit-correct').val()   == '' ||
      $('#mistake-edit-wrongex').val()   == '' ||
      $('#mistake-edit-correctex').val() == '' ||
      $('#mistake-edit-slug').val()      == ''
  ) {
    return false;
  }

  var sources = $('#mistake-edit-source').val().split('\n');

  for (i = 0; i < sources.length; i++) {
    if (sources[i] == '') continue; // ignore empty lines

    var sourceRegex = /(\w*)\:\s(.*)/; // wiktionary: https://de.wiktionary.org/wiki/Mann#Substantiv,_m
    var match = sources[i].match(sourceRegex);

    console.log('sources[' + i + '] == ' + sources[i] + '; match == null: ' + (match == null) + '; match.length = ' + match.length);
    if (match == null || match.length != 3) return false;
  }

  return true;
}


// ----------------
// CLOSE EDIT POPUP
// ----------------
function closeEditor() {
  $('.mistake-single-edit').addClass('mistake-single-edit--hidden');
  $('body').css({ "overflow-y": "auto" });
}