// -----------
// ADD MISTAKE
// -----------
function addMistake(category, mistake) {
  console.log('attempting to add mistake to ' + category, mistake);
  catMistakes[category].push(mistake);
  mergeCategories();
  sortMistakes();
}


// ------------
// MOVE MISTAKE
// ------------
function moveMistake(mistake, direction) {
  var mistakeID = $(mistake).parent().parent().parent().attr('item');
  var currentCategory = $(mistake).parent().parent().parent().parent().parent().attr('item');
  console.log('attempting to move mistake ' + mistakeID + ' in ' + currentCategory + ' ' + direction);
  var mistakeIndex = findMistakeInCategoryByID(currentCategory, mistakeID);

  // can't move up because mistake is already the first one
  if (mistakeIndex == 0 && direction == 'up') {
    return false;
  }

  // can't move down because it's already the last one
  if (mistakeIndex == catMistakes[currentCategory].length - 1 && direction == 'down') {
    return false;
  }

  var targetMistakeIndex;

  if (direction == 'up') {
    targetMistakeIndex = mistakeIndex - 1;
  } else {
    targetMistakeIndex = mistakeIndex + 1;
  }

  catMistakes[currentCategory] = swapMistakes(catMistakes[currentCategory], mistakeIndex, targetMistakeIndex);

  listIDs();
  return true; // success
}


// --------------
// DELETE MISTAKE
// --------------
function deleteMistake(category, mistakeID) {
  console.log('attempting to delete ' + mistakeID + ' in ' + category);
  var mistakeIndex = findMistakeInCategoryByID(category, mistakeID);
  var catMistakeLength = catMistakes[category].length;

  if (mistakeIndex == -1) {
    console.log("mistake " + mistakeID + " not deleted from " + category + " because it doesn't exist");
    return false;
  }

  // for all mistakes in category, beginning with the mistake to be deleted
  for (i = mistakeIndex; i < catMistakeLength - 1; i++) { // I don't know why it must be -1 here instead of -2
    catMistakes[category] = swapMistakes(catMistakes[category], i, i+1); // swap mistake to bottom
  }

  catMistakes[category].pop(); // last element == the mistake to be deleted
  listIDs();
}


// ----------------------
// SWAP MISTAKES IN ARRAY
// ----------------------
function swapMistakes(arr, i, j) {
  console.log('attempting to swap ' + i + ' with ' + j);

  // declare helper variables for later use
  var jMistake;
  var iMistake = arr[i];
  var iPath    = iMistake.path;
  var iID      = iMistake.fields.id;
  var iOrderID = iMistake.fields.orderID;
  var iName    = iMistake.fields.name;
  var iAliases = iMistake.fields.aliases;

  // swap entire mistakes
  arr[i] = arr[j];
  arr[j] = iMistake;

  // swap position-specific properties
  // IDs of j := IDs of i
  arr[j].path              = arr[i].path;
  arr[j].fields.id         = arr[i].fields.id;
  arr[j].fields.orderID    = arr[i].fields.orderID;
  arr[j].fields.name       = arr[i].fields.name;
  arr[j].fields.aliases    = arr[i].fields.aliases;

  // IDs of i := IDs of helper variables
  arr[i].path              = iPath;
  arr[i].fields.id         = iID;
  arr[i].fields.orderID    = iOrderID;
  arr[i].fields.name       = iName;
  arr[i].fields.aliases    = iAliases;

  return arr;
}