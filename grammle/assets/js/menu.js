$(".header-menu").click(function() {
  $("body").css({ "overflow-y": "hidden" });
  $(".menu").css({ "display": "block" });
  $(".header-menu").css({ "display": "none" });
  $(".header-menu-close").css({ "display": "block" });
  $(".mobile-bottom-title-div").css({ "display": "none" });
});

$(".header-menu-close").click(function() {
  $("body").css({ "overflow-y": "auto" });
  $(".menu").css({ "display": "none" });
  $(".header-menu").css({ "display": "block" });
  $(".header-menu-close").css({ "display": "none" });
  $(".mobile-bottom-title-div").css({ "display": "block" });
});

$(".home-mobile-header-menu-collapsed-button").click(function() {
  $("body").css({ "overflow-y": "hidden" });
  $(".menu").css({ "display": "block" });
  $(".home-mobile-header-menu-collapsed-button").css({ "display": "none" });
  $(".home-mobile-header-menu-collapsed-close").css({ "display": "block" });
});

$(".home-mobile-header-menu-collapsed-close").click(function() {
  $("body").css({ "overflow-y": "auto" });
  $(".menu").css({ "display": "none" });
  $(".home-mobile-header-menu-collapsed-button").css({ "display": "block" });
  $(".home-mobile-header-menu-collapsed-close").css({ "display": "none" });
});