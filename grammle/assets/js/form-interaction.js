// generate hashes for all rules
// the #hashRules element must be uncommented in the “edit” partial
// which is located in layouts/_default/edit.html as of 16 December 2022
$('#hashRules').click(function() {
  loadDefaultMistakeFile();

  setTimeout(function() {
    /*for (i = 0; i < mistakes.articles.length; i++) {
      var currentMistake = mistakes.articles[i];
      if (currentMistake.fields.uid != null && currentMistake.fields.uid.length == 32) continue;

      var date = new Date();
      var stringToBeHashed = JSON.stringify(currentMistake) + date.getTime();
      var uid = CryptoJS.MD5(stringToBeHashed).toString();

      console.log(uid);
      currentMistake.fields.uid = uid;
    }*/

    for (i = 0; i < mistakes.articles.length; i++) {
      var currentMistake = mistakes.articles[i];
      var aliases        = currentMistake.fields.aliases;
      var uid            = currentMistake.fields.uid;

      if (aliases.indexOf(uid) == -1) currentMistake.fields.aliases.push(uid);
    }

    console.log(mistakes);
  }, 2500);
});

// -----------------------
// USER LOADS DEFAULT FILE
// -----------------------
$('.default-file-button').click(function() {
  loadDefaultMistakeFile();
});


// ----------------------
// USER LOADS CUSTOM FILE
// ----------------------
$('#mistakeFile').change(function() {
  var fileReader = new FileReader();

  fileReader.onload = function() {
    loadFile(fileReader.result);
  }

  fileReader.readAsText(this.files[0]);
});


// ---------------------
// USER ADDS NEW MISTAKE
// ---------------------
$(document).on('click', '.mistake-category-addMistake', function() {
  var category = $(this).attr('item');
  console.log('attempting to add new mistake to category: ' + category);
  newMistake(category);
});


// ---------------------
// USER MOVES MISTAKE UP
// ---------------------
$(document).on('click', ".mistake-item-move--up", function() {
  moveMistake($(this), 'up');
});


// -----------------------
// USER MOVES MISTAKE DOWN
// -----------------------
$(document).on('click', ".mistake-item-move--down", function() {
  moveMistake($(this), 'down');
});


// ----------------------------
// USER CLICKS ON DELETE BUTTON
// ----------------------------
$(document).on('click', '.mistake-item-delete', function() {
  var mistakeID = $(this).parent().parent().attr('item');
  var currentCategory = $(this).parent().parent().parent().parent().attr('item');
  console.log('attempting to delete mistake ' + mistakeID + ' in category ' + currentCategory);
  deleteMistake(currentCategory, mistakeID);
});


// ---------------------------------------------
// USER CLICKS ON EDIT BUTTON FOR SINGLE MISTAKE
// ---------------------------------------------
$(document).on('click', '.mistake-item-edit', function() {
  var id = $(this).attr('item');
  var category = $(this).parent().parent().parent().parent().attr('item');
  console.log('attempting to edit mistake ' + id + ' in category ' + category);
  singleEditMistake(category, id);
});


// ----------------
// USER SAVES EDITS
// ----------------
$('.form-button-edit-save').click(function() {
  if (!(validateEdits())) {
    alert('Hoppla, da ist wohl etwas schiefgegangen! ' +
          'Überprüfe, dass du folgende Felder ausgefüllt hast: \n' +
          '\n - Slug' +
          '\n - Falsch' +
          '\n - Richtig' +
          '\n - Falsches Beispiel' +
          '\n - Richtiges Beispiel' +
          '\n - Quellen' +
          '\nQuellen müssen in der Form „quellenname: url“ angegeben werden!' +
          '\n\nBeispiel:\nwiktionary: https://de.wiktionary.org/wiki/Mann#Substantiv,_m' +
          '\n\nBeachte, dass nur bestimmte Quellen erlaubt sind. Um neue Quellen zu empfehlen, schreib eine Nachricht an den Autor (siehe Über-Seite).'
          );
    return;
  }

  console.log('attempting to save edits');

  var uid        = $('.mistake-single-edit-form').attr('grammle-uid');
  var formerID   = $('.mistake-single-edit-form').attr('grammle-id');
  var formerCat  = $('.mistake-single-edit-form').attr('grammle-category');
  var mainCat    = $('#mistake-edit-maincat').val();
  var categories = [mainCat];

  var nr   = $('.mistake-single-edit-form').attr('grammle-nr');
  var slug = $('#mistake-edit-slug').val();
  var kind = $('input[name="kind"]:checked').val();

  var wrong     = $('#mistake-edit-wrong').val();
  var wrongEx   = $('#mistake-edit-wrongex').val();
  var correct   = $('#mistake-edit-correct').val();
  var correctEx = $('#mistake-edit-correctex').val();

  // Arrays:
  var altsRaw       = $('#mistake-edit-alt').val().split('\n');
  var confusionsRaw = $('#mistake-edit-confusion').val().split('\n');
  var sourcesRaw    = $('#mistake-edit-source').val().split('\n');
  var relatedRaw    = $('#mistake-edit-related').val().split('\n');
  var aliasesRaw    = $('#mistake-edit-aliases').val().split('\n');
  var wrongsRaw     = $('#mistake-edit-wrongs').val().split('\n');

  var explanation = $('#mistake-edit-explanation').val();
  var note        = $('#mistake-edit-note').val();

  var id;
  var orderID;
  var confusion = [];
  var alt       = [];
  var sources   = [];
  var source    = [];
  var related   = [];
  var aliases   = [];
  var wrongs    = [];

  if (mainCat == formerCat) id = formerID;

  if (mainCat != formerCat) {
    var highestIDinMainCat = 0;
    var i = 0;

    // for all mistakes in main category
    for (i = 0; i < catMistakes[mainCat].length - 1; i++) {
      var currentIDstring = catMistakes[mainCat][i].fields.id;
      var currentID = parseInt(currentIDstring.slice(1));

      if (currentID > highestIDinMainCat) {
        highestIDinMainCat = currentID;
      }
    }

    highestIDinMainCat += 2; // +1 to increase, +1 due to 1-based Grammle IDs
    id = mainCat[0] + highestIDinMainCat.toString();
  }

  orderID = id.substr(0,1) + id.slice(1).padStart(4, '0'); // orderID of o24 is o0024

  // save all aliases that aren't empty
  for (i = 0; i < aliasesRaw.length; i++) {
    if (aliasesRaw[i] != '') aliases.push(aliasesRaw[i]);
  }

  aliases[0] = id; // it must be enforced that the first alias is the ID

  if (mainCat == formerCat) {
    id = formerID;
  }

  // for all selected categories
  $('input[name="category"]:checked').each(function() {
    var currentCat = $(this).val();
    // add missing new category to categories array of mistake
    if (!( categories.includes(currentCat) )) categories.push(currentCat);
  });

  // for all confusions in raw array
  for (i = 0; i < confusionsRaw.length; i++) {
    if (confusionsRaw[i] != '') confusion.push(confusionsRaw[i]);
  }

  // for all alternatives in raw array
  for (i = 0; i < altsRaw.length; i++) {
    if (altsRaw[i] != '') alt.push(altsRaw[i]);
  }

  // for all wrongs in raw array
  for (i = 0; i < wrongsRaw.length; i++) {
    if (wrongsRaw[i] != '') wrongs.push(wrongsRaw[i]);
  }

  // for all sources in raw array
  for (i = 0; i < sourcesRaw.length; i++) {
    if (sourcesRaw[i] != '') sources.push(sourcesRaw[i]);
  }

  // for all imported sources
  for (i = 0; i < sources.length; i++) {
    if (sources[i] == '') continue; // ignore empty lines

    var sourceRegex = /(\w*)\:\s(.*)/; // wiktionary: https://de.wiktionary.org/wiki/Mann#Substantiv,_m
    var match = sources[i].match(sourceRegex);

    if (match != null && match.length == 3) {
      var sourceName = match[1];
      var sourceVal = match[2];
      var sourceObj = {};

      sourceObj[sourceName] = sourceVal;
      source.push(sourceObj);
    }
  }

  related = saveRelatedMistakes(relatedRaw);

  var mistake = {
    path: id,
    fields: {
      id: id,
      uid: uid,
      orderID: orderID,
      name: "Grammle-" + id.toUpperCase(),
      slug: slug,
      aliases: aliases,
      arten: kind,
      kategorien: categories,
      wrong: wrong,
      correct: correct,
      wrongex: wrongEx,
      correctex: correctEx,
      status: new Date().getFullYear()
    }
  };

  if (alt.length > 0)       mistake.fields.alt         = alt;
  if (confusion.length > 0) mistake.fields.confusion   = confusion;
  if (source.length > 0)    mistake.fields.source      = source;
  if (related.length > 0)   mistake.fields.related     = related;
  if (wrongs.length > 0)    mistake.fields.wrongs      = wrongs;
  if (explanation != '')    mistake.fields.explanation = explanation;
  if (note != '')           mistake.fields.note        = note;

  // unique ID of a mistake := hash of .toString() + milliseconds since 1970.
  // The UID must never change, because it's used to link related mistakes together.
  //
  // ======
  //
  // Assume we linked rules G3 and G8 only based on their respective IDs.
  // Further assume we'd use the web-based edit tool to re-order several mistakes, including G3 or G8 (or both).
  //
  // Now, because G8 isn't G8 anymore – as it was re-ordered as G7 –
  // we'd need to figure out a way to re-link it to G3.
  //
  // The problem is: We don't know whether G3 is still G3
  // – perhaps it had been re-ordered itself before G8 became G7.
  //
  // The only solution that is guaranteed to always work
  // is to create truly unique IDs for each mistake at the time of creation.
  //
  // That doesn't mean it's impossible to link mistakes based on their IDs,
  // but this would be way to complicated to implement, maintain and refactor.
  //
  // Thus, we're using cryptographic hashes of the mistake objects plus the respective current date.
  // Obviously, md5 is outdated, but since our use case is not sensitive at all,
  // that's perfectly ok, or rather it's not worth wasting significantly more space just for SHA512.
  //
  // ======

  // Any UID must have a length of 32 characters (that's per MD5.toString())
  if (uid.length != 32) {
    var hashString = JSON.stringify(mistake) + new Date().getTime().toString(); // hash of mistake + date in milliseconds
    console.log("calculating MD5 hash of " + hashString);
    uid = CryptoJS.MD5(hashString).toString();
    console.log("MD5 hash of " + hashString + ": " + uid);
    mistake.fields.uid = uid;
  }

  if (aliases.indexOf(mistake.fields.uid) == -1) aliases.push(mistake.fields.uid);

  console.log('attempting to save mistake ' + id + ' in category ' + mainCat);

  if (formerCat != mainCat) {
    // In order to move a mistake to a different category,
    // we basically delete the mistake in the old category
    // and add it to its new category.
    console.log('attempting to delete mistake ' + formerID + ' in category ' + formerCat);
    deleteMistake(formerCat, formerID);

    // That obviously means the mistake's ID will change.
    // Both the category letter and the number must be re-generated.
    var highestIDinCategory = getHighestIDinCategory(catMistakes[mainCat]); // get current highest ID in new category
    var categoryLetter  = highestIDinCategory.slice(0,1); // category letter of O43 is O
    var newIDnumber   = parseInt(highestIDinCategory.slice(1)) + 1; // O43 -> 43 -> 44
    var newID         = categoryLetter + newIDnumber; // O + 44 = O44
    mistake.fields.id = newID; // new highest ID == current highest ID + 1

    // Add the mistake to its new category.
    console.log('attempting to add mistake ' + newID + ' in category ' + mainCat);
    addMistake(mainCat, mistake);
  }

  if (formerCat == mainCat) {
    var indexOfMistakeInMainCat = getIndexOfMistakeInCategory(catMistakes[mainCat], mistake.fields.id);

    // if old mistake couldn't be found because mistake is new
    if (indexOfMistakeInMainCat == -1) {
      var highestIDinCategory = getHighestIDinCategory(catMistakes[mainCat]);
      indexOfMistakeInMainCat = parseInt(highestIDinCategory.slice(1));
      // The above command must be explained a bit. Don't think you understand what it does; you don't.
      // A mistake's ID is ONE-based, so the slice operation will return a ONE-based index.
      // The actual index of the mistake in the array, however, is ZERO-based.
      // In theory, we would have to account for this by incrementing it.
      // However, due to the -1 we know that the mistake is not present in the array.
      // Instead of ID-- and index++ we can simply do index := ID. The result is the same.
    }

    catMistakes[mainCat][indexOfMistakeInMainCat] = mistake;
    mergeCategories();
  }

  listIDs();
  closeEditor();
  toast('success', 'Gespeichert!');
});


function saveRelatedMistakes(relatedRaw) {
  console.log('attempting to save related mistakes ' + relatedRaw);
  var related = [];
  var i = 0;

  // for all related rules in raw array
  for (i = 0; i < relatedRaw.length; i++) {
    if (relatedRaw[i] == '') continue; // skip iteration if entry is empty

    var currentItem = relatedRaw[i];
    var uid = currentItem;

    // Any UID must have a length of 32 characters (that's per MD5.toString())
    if (currentItem.length < 32) uid = getUIDbyID(currentItem);
    if (currentItem.length > 32) continue;

    related.push(uid);
  }

  return related;
}


// ------------------------------------
// USER CLICKS ON EDITOR’S CLOSE BUTTON
// ------------------------------------
$('.mistake-single-edit-headline-close, .form-button-edit-cancel').click(function() { // previously: .mistake-single-close
  closeEditor();
});


// ---------------------------
// USER CLICKS GENERATE BUTTON
// ---------------------------
$('.file-output-button').click(function() {
  if ($(this).hasClass('form-button--disabled')) return;
  console.log('attempting to generate output file');
  mergeCategories();
  generateMistakesFile();
});