// ------------------------
// GET DEFAULT MISTAKE FILE
// ------------------------
function loadDefaultMistakeFile() {
  $.ajax({
    url: "/mistakes.yaml"

  }).done(function(file) {
    loadFile(file);

  }).fail(function(jqXHR, textStatus, errorThrown) {
    alert('Upps, etwas hat nicht geklappt! Womöglich hast du gerade keine Internet-Verbindung. ' +
          'Lade die Seite neu oder wähle eine eigene Grammle-Fehlerdatei aus. ' +
          'Die aktuelle Datei kannst du von Codeberg herunterladen, siehe „Über“. \n\n' +
          textStatus + ": " + errorThrown);
  });
}


// ---------
// LOAD FILE
// ---------
function loadFile(file) {
  var mistakesYAML;
  mistakes = {};
  catMistakes = {};
  mistakesYAML = file;

  try {
    mistakes = jsyaml.load(mistakesYAML);
    sortMistakes();
    listIDs();

  } catch(error) {
    alert("Upps, etwas hat nicht geklappt! Wahrscheinlich ist deine Datei fehlerhaft. \n\n" + error);
  }
}

// ----------------------
// OUTPUT FINAL YAML FILE
// ----------------------
function generateMistakesFile() {
  var content = jsyaml.dump(mistakes);
  var filename = "mistakes.yaml";

  var blob = new Blob([content], {
   type: "text/plain;charset=utf-8"
  });

  saveAs(blob, filename);
}