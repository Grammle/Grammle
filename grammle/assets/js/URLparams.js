const urlParams = new URLSearchParams(window.location.search);

if (urlParams.has('kategorie') &&
    urlParams.has('id'))
{
  loadParamID(urlParams.get('kategorie'), urlParams.get('id'));
}

function loadParamID(category, id) {
  loadDefaultMistakeFile();
  // Apparently, we need to wait a few moments before opening the edit popup,
  // because otherwise we get the following error in the console:
  // Uncaught TypeError: catMistakes[category] is undefined
  setTimeout(singleEditMistake, 500, category, id);
}