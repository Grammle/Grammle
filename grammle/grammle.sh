#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

yaml() {
  python3 -c "import yaml;print(yaml.safe_load(open('$1'))$2)"
}

hugoNormal=true
commitMode=false


# Is the first argument "serve"?
if (( $# > 0 )); then
  if [[ "$1" == "serve" || "$1" == "server" ]]; then
    hugoNormal=false
  fi
fi

if (( $# > 0 )); then
  if [[ "$1" == "commit" ]]; then
    commitMode=true
  fi
fi

# Does folder content/regeln exist? If so, delete it.
if [[ -d "content/regeln" ]]; then
  printf "removing folder 'content/regeln'...\n\n"
  trash-put content/regeln
fi

node hugo.js generate
echo ""

if [[ $hugoNormal == true ]]; then
  # Get name of publish directory from config.yaml
  printf "Reading publishDir from config.yaml: "
  publishDir=$(yaml config.yaml "['publishDir']")
  printf "\e[32m\e[1m$publishDir\e[0m\n"

  # get SHA256 hash of current date
  now=`date +"%Y-%m-%d--%H:%M:%s:%N"`
  tempFolder=($(echo -n "$now" | sha256sum))

  # copy .git folder from publish dir to temp folder
  echo "Backing up Git repo of publishDir..."
  mkdir $tempFolder
  cp -r $publishDir/.git $tempFolder

  # empty publish dir and re-add .git folder
  echo "Emptying publishDir..."
  trash-put $publishDir
  mkdir $publishDir
  echo "Restoring Git repo..."
  cp -r $tempFolder/.git $publishDir

  # remove temp folder
  trash-put $tempFolder
fi

if [[ $commitMode == false ]]; then
  hugo "$@"
else
  hugo
  bash commit.sh
fi
