---
title: Änderungsprotokoll
layout: changelog
aliases:
  - änderungen
outputs:
  - HTML
  - feed
---

# Änderungs&shy;protokoll

Hier ist eine chronologische Liste aller Änderungen, die an Grammle über die Zeit vorgenommen wurden. Wenn du benachrichtigt werden möchtest, sobald Regeln hinzugefügt werden oder neue Funktionen eingeführt werden, kannst du den [RSS-Feed](https://grammle.de/aenderungen/index.xml "Feed-URL") abonnieren.

Was ist ein RSS-Feed und wie abonniere ich ihn? [Mehr Infos](https://corporate.dw.com/de/was-ist-rss-wof%C3%BCr-brauche-ich-rss-und-wie-abonniere-ich-rss-feeds-der-deutschen-welle/a-2115912 "Deutsche Welle")

{{< feedButton >}}