---
title: Über Grammle
layout: meta
aliases:
  - über
  - about
---

# Häufig gestellte Fragen

{{< margin "big" >}}

{{< grammleBanner >}}

## Wozu ist Grammle gut?

Grammle bietet eine kurze und knackige Übersicht über häufige sprachliche Fehler. Damit soll Grammle es ermöglichen, eigenen sprachlichen Unsicherheiten zu begegnen. Fragt man sich regelmäßig, ob es „aufwändig“ oder „aufwendig“ heißt, könnte man sich einfach die entsprechende Regel als Lesezeichen abspeichern.

Grammle kann auch beim Korrekturlesen fremder Texte hilfreich sein, um Änderungen von Formulierungen kurz und prägnant zu begründen. Statt lang und breit selbst darlegen zu müssen, warum ein bestimmter Ausdruck lieber vermieden werden sollte, kann der Lektor einfach auf die entsprechende Grammle-Regel verweisen.

## Welche Kategorien gibt es?

Jedem Grammle-Eintrag ist mindestens eine dieser [Kategorien](/kategorien/) zugeordnet:

{{< kategorien >}}

{{< margin "big" >}}

## Welche Fehlerarten gibt es?

Grammle unterscheidet zwischen folgenden [Fehlerarten](/arten/):

{{< arten >}}

Unter **Fehler („falsch“)** fallen all jene Ausdrücke, die zweifelsfrei als inkorrekt eingestuft werden können. Als **Warnung** ist gelistet, was im Allgemeinen als **„eher falsch“** angesehen wird oder erst im Zuge der Rechtschreibreform erlaubt bzw. aufgeweicht wurde. Insbesondere stilistische Regeln sind als **Informationen („nicht empfohlen“)** bei Grammle zu finden.

## Wofür stehen die Kürzel?

Jede Grammle-Regel hat ein eigenes Kürzel, das sich aus ihrer (erstgenannten) Kategorie und einer Zahl zusammensetzt, z.B. [Grammle-S2]({{< ref "/regeln/s2" >}}) aus der Kategorie Sprachstil oder [Grammle-G4]({{< ref "/regeln/g4" >}}) aus der allgemeinen Kategorie Grammatik.

## Woran orientieren sich die Grammle-Regeln?

Bei Grammle handelt es sich nicht bloß um eine wiedergekäute Sammlung von Grammatikregeln aus anderen Quellen. Zwar orientiert sich Grammle an Standardwerken zur deutschen Sprache, allen voran natürlich am Duden, und berücksichtigt allgemein etablierte Rechtschreibregeln. Doch insbesondere was die Themensetzung bzw. Relevanz der Einträge angeht, erlaubt sich Grammle durchaus ein eigenes Urteil – nach der persönlichen Einschätzung des Autors. Das ist beispielsweise dann der Fall, wenn im Duden mehrere mögliche Schreibweisen gelistet sind.

Grammle-Regeln, welche der Konvention widersprechen, sind natürlich immer detailliert begründet. Zudem ist zu jedem Eintrag ein Verweis auf weiterführende Quellen angegeben.

## Wie bleibe ich auf dem Laufenden?

Ganz einfach: Du kannst Grammles [Änderungsprotokoll]({{< ref "/aenderungen/" >}}) als [RSS-Feed]({{< ref "/aenderungen" >}}/index.xml "Feed-URL") in deinem Feedreader abonnieren. Empfehlenswert sind [Feeder](https://f-droid.org/packages/com.nononsenseapps.feeder "Feeder auf F-Droid") und – falls du schon eine [NextCloud](https://nextcloud.com "nextcloud.com") hast – [NextCloud News](https://f-droid.org/packages/de.luhmer.owncloudnewsreader "NextCloud News auf F-Droid").

{{< feedButton >}}

## Kann ich mitmachen?

Klar! Feedback, Kritik und Ergänzungen sind immer willkommen, und auch Vorschläge für ganz neue Grammle-Einträge kannst du gern machen. Gehe dazu zur Bearbeitungsseite, speichere deine Änderungen in eine Datei und sende diese an den Autor.

{{< editWidget >}}

## Wie wurde Grammle erstellt?

Die Informationen zu den einzelnen Regeln sind im YAML-Format gespeichert und werden zunächst mithilfe von [Asaf Zamirs](https://www.kidsil.net "Asafs Website") `hugo.js` aus [Hugo-Data-to-Pages](https://github.com/kidsil/hugo-data-to-pages "GitHub-Repo") in einzelne Markdown-Seiten umgewandelt. Anschließend wird daraus mit dem fantastischen Static-Site-Generator [Hugo](https://gohugo.io "gohugo.io") die Website fertiggebaut.

`hugo.js` wird leicht abgewandelt gemäß der [MIT-Lizenz](https://github.com/kidsil/hugo-data-to-pages/blob/master/LICENSE.md "Lizenz auf GitHub") verwendet. Grammle nutzt zudem Icons von [Bootstrap](https://icons.getbootstrap.com "icons.getbootstrap.com") gemäß der [MIT-Lizenz](https://github.com/twbs/icons/blob/main/LICENSE.md "Lizenz auf GitHub") und von [Font Awesome](https://fontawesome.com "fontawesome.com") nach deren [eigener Lizenz](https://github.com/FortAwesome/Font-Awesome/blob/6.x/LICENSE.txt "Lizenz auf GitHub").

## Ist Grammles Quellcode einsehbar?

Ja, die Grammle-Website ist quelloffen, und das Git-Repository findest du auf [Codeberg](https://codeberg.org/Grammle/Grammle "codeberg.org/Grammle/Grammle").

{{< codebergBanner >}}

## Darf ich Grammle selbst weiterverwenden?

Das gesamte Grammle-Repository (also Inhalte und Quellcode) darf gemäß der ethischen Lizenz [For Good Eyes Only v0.2](https://codeberg.org/Grammle/Grammle#lizenzen "Readme.md") verwendet werden, die eine freie und kostenlose Weiterverwendung unter bestimmten Bedingungen erlaubt, sowie unter einigen Voraussetzungen gemäß einer [Creative-Commons-Lizenz](https://codeberg.org/Grammle/Grammle#lizenzen "Readme.md").

{{< fogeye >}}

## Wie wird Grammle gehostet?

Grammle ist eine statische HTML-Seite, die von [Codeberg Pages](https://codeberg.page "codeberg.page") gehostet wird. Der gemeinnützige Codeberg e.V. fördert freie Software, indem er eine kostenlose Plattform anbietet, auf der Entwickler ihren Quellcode veröffentlichen können.

In Codebergs [Satzung](https://codeberg.org/Codeberg/org/src/branch/main/Satzung.md#2-ziele-und-aufgaben) heißt es:
>**(1)** Zweck des Vereins ist es, die Schaffung, Sammlung, Verbreitung und Bewahrung Freier Inhalte (Free Content, Open Content, Free Cultural Work) und Freier und Offener Software (Free and Open Source Software, FOSS), und deren Dokumentation in selbstloser Tätigkeit zu fördern, um damit die Chancengleichheit beim Zugang zu Wissen und Bildung zu ermöglichen. Dazu soll auch das Bewusstsein für die damit zusammenhängenden gesellschaftlichen und philosophischen Fragen geschärft werden.

Vielen Dank, Codeberg! ❤️

## Welche Nutzerdaten sammelt Grammle?

Grammle sammelt selbst **keinerlei Nutzerdaten**, da es sich bloß um eine statische HTML-Website handelt, die keine Cookies oder Tracker verwendet. Auf dem Codeberg-Pages-Server fallen technisch notwendigerweise in der Protokolldatei die **IP-Adresse** und der **User-Agent** jedes Besuchers an.

Damit du eine **Website** (z.B. Grammle) besuchen kannst, muss dein **Webclient** (z.B. Firefox-Browser) eine Verbindung mit dem **Webserver** (z.B. Codeberg Pages) herstellen und die Seiteninhalte (Text, Grafiken, Styles, Skripte) anfragen. Damit diese auch **tatsächlich** beim Client **ankommen**, muss dieser dem Server seine Adresse (IP-Adresse) mitteilen.

(Das ist so ähnlich, wie wenn du einen Brief schreibst und vom Absender eine Antwort erwartest. Damit er seine Antwort abschicken kann, braucht er die Absenderadresse.)

Wenn du deine **IP-Adresse nicht preisgeben** möchtest, empfiehlt Grammle den **[Tor-Browser](https://torproject.org "torproject.org")**, der das anonyme [Tor-Netzwerk](https://digitalcourage.de/support/tor "Digitalcourage-Artikel") als „Zwischenhändler“ zwischen dir und allen Websites verwendet. Praktisch: Dadurch verschleierst du auch deinem Internetprovider, deiner Schule oder Uni sowie NSA & Co. gegenüber zuverlässig, welche Websites du besuchst.

Der User-Agent ist technisch nicht zwingend erforderlich, allerdings liegt es in der Hand des Clients, ihn zu übermitteln (oder eben nicht).

Diese Daten werden jedoch spätestens nach **sieben Tagen** gelöscht. Mehr Informationen dazu findest du in [Codebergs Datenschutzbestimmungen](https://codeberg.org/Codeberg/org/src/branch/main/PrivacyPolicy.md#ip-logging).

## Wer ist der Server-Betreiber?

Der Server, welcher Grammle hostet, wird vom wundervollen [Codeberg e.V.](https://codeberg.org "codeberg.org") betrieben, dessen Impressum unter [codeberg.org](https://codeberg.org/codeberg/org/src/branch/main/Imprint.md#impressum-nach-5-tmg-imprint-according-to-german-law) zu finden ist.

## Wer ist der Autor?

Grammle wurde von [Pixelcode](https://keyoxide.org/AEEF8CDB5ADF2F28016F39E1FBFC237DAF98D402 "Keyoxide-Profil") erstellt. Er entscheidet über die Sortierung und Relevanz der Regeln. Hobbymäßig entwickelt er freie und quelloffene Software auf der Git-Plattform [Codeberg](https://codeberg.org "codeberg.org"). Folge ihm auf [Mastodon](https://mstdn.social/@pixelcodeapps "@pixelcodeapps@mstdn.social")!

{{< contactWidget "mastodon" >}}

{{< margin >}}

{{< contactWidget "codeberg" >}}

{{< margin >}}

{{< contactWidget "keyoxide" >}}

{{< margin >}}

## Was soll das gelbe Wappen auf der Startseite?

Seit dem 24. Februar 2022 tobt in der Ukraine ein tödlicher [Vernichtungskrieg](https://www.n-tv.de/politik/Russland-fuehrt-wirklich-einen-Vernichtungskrieg-article23358029.html "n-tv") gegen die Zivilbevölkerung, grundlos angezettelt vom russischen Staat. Je mehr Quadratkilometer durch die ukrainischen Streitkräfte [zurückerobert](https://www.dw.com/de/ukraine-aktuell-ukraine-verdr%C3%A4ngt-russische-k%C3%A4mpfer-aus-lyman/a-63303840 "Deutsche Welle") und damit befreit werden, desto mehr [Massengräber](https://www.tagesschau.de/ausland/europa/ukraine-lyman-massengrab-101.html "Tagesschau"), [Folterkammern](https://www.sueddeutsche.de/politik/krieg-ukrainische-polizei-entdeckt-mehrere-folterkammern-dpa.urn-newsml-dpa-com-20090101-220915-99-780237 "Süddeutsche") und sonstige Zeugnisse der blutrünstigen Invasionstruppen Putins erschüttern die ganze Welt. Selbst vor Kindern, Alten und anderen Wehrlosen macht die Barbarei nicht halt.

Hilf mit, dem Morden, [Foltern](https://www.spiegel.de/ausland/ukraine-krieg-im-folterknast-von-balaklija-a-37e58c67-07ff-4924-bdcc-da2e090de167 "Spiegel"), [Vergewaltigen](https://www.spiegel.de/ausland/ukraine-krieg-russische-soldaten-sollen-auch-maenner-und-jungen-vergewaltigt-haben-a-674a632b-0390-40e3-b5b8-9c1f9cb01b4f "Spiegel") und [Brandschatzen](https://www.rnd.de/politik/ukraine-krieg-russlands-soldaten-pluendern-staedte-kameras-nehmen-alles-auf-OQL6GUC3XNA73IU5T7QHOWJN2M.html "RND") ein Ende zu bereiten!

Jede Spende an die Ukraine unterstützt [humanitäre Hilfe](https://www.dw.com/de/ukraine-aktuell-140000-menschen-in-charkiw-brauchen-laut-un-hilfe/a-63365049 "Deutsche Welle") für die unschuldigen Zivilisten, den [Wiederaufbau](https://u24.gov.ua/needs "u24.gov.ua") zerstörter Städte und Infrastruktur sowie die Beschaffung entscheidender [Waffen](https://www.bbc.com/news/world-europe-63121649 "BBC"), mithilfe derer der Krieg verkürzt werden kann.

Bitte [spende](https://u24.gov.ua "u24.gov.ua") gegen den Krieg.

{{< uaButton >}}