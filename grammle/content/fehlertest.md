---
title: Fehlertest
layout: fehlertest
---

# Fehlertest

Der folgende Text enthält Beispiele aller auf Grammle gelisteten Fehler und dient zum Ausprobieren des Browser-Addons von Grammle, welches Internetseiten auf Fehler scannt und diese dann markiert.

Beachte, dass Grammle Fehler nicht erkennen kann (selbst wenn sie in der Liste sind), wenn

* sie (z.B. durch Getrenntschreibung) über den ganzen Satz verstreut sind,
* ihr Kasus ohne intelligentes Verständnis des Kontextes nicht eindeutig bestimmbar ist,
* sie von der jeweiligen Seite auf unvorhersehbare Weise formatiert sind oder
* sie bedingte Trennstriche (sog. „soft hyphens“) enthalten.

Die Grammle-Fehlererkennung arbeitet nicht mit „künstlicher Intelligenz“, sondern mithilfe ganz simpler regulärer Ausdrücke („Regex“, regular expressions), weshalb die Erwartungen nicht zu hoch sein sollten.

Das Addon kann heruntergeladen werden, sobald es fertiggestellt ist.

## Test-Text:

{{< margin "big" >}}